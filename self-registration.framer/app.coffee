# Define and set custom device
Framer.Device.customize
	deviceType: Framer.Device.Type.Computer
	screenWidth: 1920
	screenHeight: 1080
	deviceImage: "http://f.cl.ly/items/001L0v3c1f120t0p2z24/custom.png"
	deviceImageWidth: 1920
	deviceImageHeight: 1080


Canvas.backgroundColor = "#F3F3F3"

ViewController = require 'ViewController'
{ƒ,ƒƒ} = require 'findModule'

# Import file "self-registration"
sketch = Framer.Importer.load("imported/self-registration@1x")

Views = new ViewController
   initialView: sketch.Login_screen_landing_

# Define elements
register = sketch.Registration_form
registerInput = sketch.email_input
registerBtn = sketch.btn_create_your_account
login = sketch.Login_form
loginEmailInput = sketch.email_address_input
loginPasswordInput = sketch.enter_password_input
loginShowPasswordBtn = sketch.btn_toggle_password
divider = sketch.Divider
btnResetPassword = sketch.btn_reset_password

# Define element states
register.states =
	inactive:
		y: Align.bottom(-20)
		opacity:0
	active: 
		y: Align.center

registerBtn.states =
	inactive:
		opacity:0
		visible: true
	active:
		opacity:1

login.states = 
	inactive:
		y: -20
		opacity:0
	active: 
		y: Align.center

divider.states = 
	hidden:
		opacity:0

registerBtn.visible = true
registerBtn.states.switchInstant "inactive"

# Define animationOptions
register.animationOptions =
	curve: Spring(damping: 0.5)
	
login.animationOptions =
	curve: Spring(damping: 0.5)

divider.animationOptions =
	curve: Spring(damping: 0.5)
	
registerBtn.animationOptions =
	curve: Spring(damping: 0.5)

# Switch states on click
registerInput.onClick ->
	register.states.switch "active"
	login.states.switch "inactive"
	divider.states.switch "hidden"
	registerBtn.states.switch "active"

loginEmailInput.onClick ->
	login.states.switch "active"
	register.states.switch "inactive"
	divider.states.switch "hidden"

loginPasswordInput.onClick ->
	login.states.switch "active"
	register.states.switch "inactive"
	divider.states.switch "hidden"

sketch.Canvas.onClick ->
	login.states.switch "default"	
	register.states.switch "default"
	divider.states.switch "default"
	registerBtn.states.switch "inactive"

btnResetPassword.onClick ->
	# reset password view trigger

registerBtn.onClick (event, layer) ->
	Views.moveInUp(sketch.$2factor)

