// This is autogenerated by Framer


if (!window.Framer && window._bridge) {window._bridge('runtime.error', {message:'[framer.js] Framer library missing or corrupt. Select File → Update Framer Library.'})}
window.__imported__ = window.__imported__ || {};
window.__imported__["self-registration@1.5x/layers.json.js"] = [
	{
		"objectId": "51B20B96-65DF-43DF-B1CE-3AEF66F3F854",
		"kind": "artboard",
		"name": "Login_screen_landing_",
		"originalName": "Login screen (landing)",
		"maskFrame": null,
		"layerFrame": {
			"x": -512,
			"y": -483,
			"width": 1920,
			"height": 1080
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"image": {
			"path": "images/Layer-Login_screen_landing_-ntfcmjbc.png",
			"frame": {
				"x": -512,
				"y": -483,
				"width": 1920,
				"height": 1080
			}
		},
		"children": [
			{
				"objectId": "C882E7EE-8067-4732-80DF-E9575F373072",
				"kind": "group",
				"name": "Login_form",
				"originalName": "Login form",
				"maskFrame": null,
				"layerFrame": {
					"x": 602,
					"y": 164,
					"width": 721,
					"height": 404
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Login_form-qzg4mku3.png",
					"frame": {
						"x": 602,
						"y": 164,
						"width": 721,
						"height": 404
					}
				},
				"children": [
					{
						"objectId": "5AA549EF-1052-416F-886E-681984AFF618",
						"kind": "group",
						"name": "Group_1",
						"originalName": "Group_1",
						"maskFrame": null,
						"layerFrame": {
							"x": 1098,
							"y": 440,
							"width": 183,
							"height": 23
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_1-nufbntq5.png",
							"frame": {
								"x": 1098,
								"y": 440,
								"width": 183,
								"height": 23
							}
						},
						"children": [
							{
								"objectId": "6EC4BCB6-BC67-4FD1-BC2D-B5C075EE38FC",
								"kind": "text",
								"name": "Reset_your_password",
								"originalName": "Reset_your_password",
								"maskFrame": null,
								"layerFrame": {
									"x": 1130,
									"y": 444.9999999999999,
									"width": 151,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "Reset your password",
									"css": [
										"/* Reset your password: */",
										"font-family: Lato-Black;",
										"font-size: 16px;",
										"color: #000000;"
									]
								},
								"image": {
									"path": "images/Layer-Reset_your_password-nkvdnejd.png",
									"frame": {
										"x": 1130,
										"y": 444.9999999999999,
										"width": 151,
										"height": 16
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "3219C27D-650C-47C0-AA42-39FC12877D1C",
						"kind": "group",
						"name": "Group_2",
						"originalName": "Group_2",
						"maskFrame": null,
						"layerFrame": {
							"x": 647,
							"y": 442,
							"width": 226,
							"height": 21
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2-mzixoumy.png",
							"frame": {
								"x": 647,
								"y": 442,
								"width": 226,
								"height": 21
							}
						},
						"children": [
							{
								"objectId": "92B6DF09-F04D-4ABE-8E0B-0EF2CDECB997",
								"kind": "text",
								"name": "Remember_my_login_details",
								"originalName": "Remember_my_login_details",
								"maskFrame": null,
								"layerFrame": {
									"x": 676,
									"y": 444.9999999999999,
									"width": 197,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "Remember my login details",
									"css": [
										"/* Remember my login details: */",
										"font-family: Lato-Black;",
										"font-size: 16px;",
										"color: #000000;"
									]
								},
								"image": {
									"path": "images/Layer-Remember_my_login_details-otjcnkrg.png",
									"frame": {
										"x": 676,
										"y": 444.9999999999999,
										"width": 197,
										"height": 16
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "AABC330F-045C-43F9-98F1-7D0C07F2FB7B",
						"kind": "text",
						"name": "Your_health_journey_starts_here",
						"originalName": "Your_health_journey_starts_here",
						"maskFrame": null,
						"layerFrame": {
							"x": 713,
							"y": 164,
							"width": 507,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "Your health journey starts here",
							"css": [
								"/* Your health journey starts here: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Your_health_journey_starts_here-qufcqzmz.png",
							"frame": {
								"x": 713,
								"y": 164,
								"width": 507,
								"height": 35
							}
						},
						"children": []
					},
					{
						"objectId": "745071AA-F9CA-4B3F-AA24-A408471410E9",
						"kind": "text",
						"name": "LOGIN",
						"originalName": "LOGIN",
						"maskFrame": null,
						"layerFrame": {
							"x": 920,
							"y": 520,
							"width": 79,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "LOGIN",
							"css": [
								"/* LOGIN: */",
								"font-family: OpenSans-Bold;",
								"font-size: 26px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-LOGIN-nzq1mdcx.png",
							"frame": {
								"x": 920,
								"y": 520,
								"width": 79,
								"height": 20
							}
						},
						"children": []
					},
					{
						"objectId": "B0E3BF0D-3895-4F6D-825A-082B8FD16ACF",
						"kind": "text",
						"name": "Enter_your_password",
						"originalName": "Enter_your_password",
						"maskFrame": null,
						"layerFrame": {
							"x": 648,
							"y": 372,
							"width": 179,
							"height": 19
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Enter your password",
							"css": [
								"/* Enter your password: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Enter_your_password-qjbfm0jg.png",
							"frame": {
								"x": 648,
								"y": 372,
								"width": 179,
								"height": 19
							}
						},
						"children": []
					},
					{
						"objectId": "179FE9B0-BEE9-4780-BD92-88A706AC493C",
						"kind": "group",
						"name": "Rectangle_2",
						"originalName": "Rectangle_2",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 341,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_2-mtc5rku5.png",
							"frame": {
								"x": 602,
								"y": 341,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "475EBC64-0E20-497C-A991-0AD42D4D39CB",
						"kind": "text",
						"name": "Enter_your_email_address_2",
						"originalName": "Enter_your_email_address-2",
						"maskFrame": null,
						"layerFrame": {
							"x": 648,
							"y": 260,
							"width": 214,
							"height": 19
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Enter your email address",
							"css": [
								"/* Enter your email address: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Enter_your_email_address_2-ndc1rujd.png",
							"frame": {
								"x": 648,
								"y": 260,
								"width": 214,
								"height": 19
							}
						},
						"children": []
					},
					{
						"objectId": "83443C9E-9122-469B-B700-E63C0E5B8058",
						"kind": "group",
						"name": "Rectangle_1",
						"originalName": "Rectangle_1",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 229,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_1-odm0ndnd.png",
							"frame": {
								"x": 602,
								"y": 229,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "83244D48-7DFF-4604-80D5-E12C08B64C76",
				"kind": "group",
				"name": "Registration_form",
				"originalName": "Registration form",
				"maskFrame": null,
				"layerFrame": {
					"x": 602,
					"y": 714,
					"width": 721,
					"height": 186
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "B9FDDEFC-00C1-43AE-916E-F87CAF9CFD71",
						"kind": "text",
						"name": "Enter_your_email_address",
						"originalName": "Enter_your_email_address",
						"maskFrame": null,
						"layerFrame": {
							"x": 648,
							"y": 810,
							"width": 214,
							"height": 19
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Enter your email address",
							"css": [
								"/* Enter your email address: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Enter_your_email_address-qjlgrerf.png",
							"frame": {
								"x": 648,
								"y": 810,
								"width": 214,
								"height": 19
							}
						},
						"children": []
					},
					{
						"objectId": "F0AD7474-1D3F-4F13-9F6D-91D41A631B74",
						"kind": "group",
						"name": "Rectangle_4",
						"originalName": "Rectangle_4",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 779,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_4-rjbbrdc0.png",
							"frame": {
								"x": 602,
								"y": 779,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "50B6C109-17F9-4928-8028-0B16E2B85675",
						"kind": "group",
						"name": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_",
						"originalName": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account.",
						"maskFrame": null,
						"layerFrame": {
							"x": 681,
							"y": 874,
							"width": 517,
							"height": 26
						},
						"visible": true,
						"metadata": {
							"opacity": 0.92
						},
						"image": {
							"path": "images/Layer-Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_-ntbcnkmx.png",
							"frame": {
								"x": 681,
								"y": 874,
								"width": 517,
								"height": 26
							}
						},
						"children": []
					},
					{
						"objectId": "5B80874D-FB26-46AD-941B-62A1DEB468F5",
						"kind": "text",
						"name": "New_to_member_portal_Register_here",
						"originalName": "New_to_member_portal_Register_here",
						"maskFrame": null,
						"layerFrame": {
							"x": 664,
							"y": 714,
							"width": 609,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "New to member portal? Register here",
							"css": [
								"/* New to member portal? Register here: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-New_to_member_portal_Register_here-nui4mdg3.png",
							"frame": {
								"x": 664,
								"y": 714,
								"width": 609,
								"height": 35
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "F2E98239-734D-4B67-8310-3196F996E891",
		"kind": "artboard",
		"name": "Login_screen_register_activce_",
		"originalName": "Login screen (register activce)",
		"maskFrame": null,
		"layerFrame": {
			"x": 1508,
			"y": -483,
			"width": 1920,
			"height": 1080
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "0FB5E997-6A38-4728-B016-0E08EA98A724",
				"kind": "group",
				"name": "Login_screen_register_active_",
				"originalName": "Login screen (register active)",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1920,
					"height": 1080
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Login_screen_register_active_-mezcnuu5.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1920,
						"height": 1080
					}
				},
				"children": [
					{
						"objectId": "3B580AC5-BAFF-4D9F-98E1-22E90DF4F1AD",
						"kind": "text",
						"name": "Email_address",
						"originalName": "Email_address",
						"maskFrame": null,
						"layerFrame": {
							"x": 645,
							"y": 487,
							"width": 125,
							"height": 17
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Email address",
							"css": [
								"/* Email address: */",
								"font-family: Lato-Black;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Email_address-m0i1odbb.png",
							"frame": {
								"x": 645,
								"y": 487,
								"width": 125,
								"height": 17
							}
						},
						"children": []
					},
					{
						"objectId": "B39E63B4-366B-4ED1-B300-09AA50AE68D8",
						"kind": "group",
						"name": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_1",
						"originalName": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account.",
						"maskFrame": null,
						"layerFrame": {
							"x": 681,
							"y": 611,
							"width": 517,
							"height": 26
						},
						"visible": true,
						"metadata": {
							"opacity": 0.92
						},
						"image": {
							"path": "images/Layer-Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_-qjm5rtyz.png",
							"frame": {
								"x": 681,
								"y": 611,
								"width": 517,
								"height": 26
							}
						},
						"children": []
					},
					{
						"objectId": "87613974-3455-43E2-A970-FB150E11E19E",
						"kind": "text",
						"name": "jkonto_gmail_com",
						"originalName": "jkonto_gmail.com",
						"maskFrame": null,
						"layerFrame": {
							"x": 645,
							"y": 547,
							"width": 161,
							"height": 19
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "jkonto@gmail.com",
							"css": [
								"/* jkonto@gmail.com: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-jkonto_gmail_com-odc2mtm5.png",
							"frame": {
								"x": 645,
								"y": 547,
								"width": 161,
								"height": 19
							}
						},
						"children": []
					},
					{
						"objectId": "69351AAF-C3FF-44B6-8871-8A2EBEEA322E",
						"kind": "group",
						"name": "Rectangle_41",
						"originalName": "Rectangle_4",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 516,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_4-njkzntfb.png",
							"frame": {
								"x": 602,
								"y": 516,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "DE00769D-3DB0-4A6C-A000-2AE631FA2351",
						"kind": "text",
						"name": "New_to_member_portal_Register_here1",
						"originalName": "New_to_member_portal_Register_here",
						"maskFrame": null,
						"layerFrame": {
							"x": 664,
							"y": 407,
							"width": 609,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "New to member portal? Register here",
							"css": [
								"/* New to member portal? Register here: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-New_to_member_portal_Register_here-reuwmdc2.png",
							"frame": {
								"x": 664,
								"y": 407,
								"width": 609,
								"height": 35
							}
						},
						"children": []
					},
					{
						"objectId": "CAB71B8C-781C-42AC-A06D-2F0A44FB5568",
						"kind": "text",
						"name": "CREATE_YOUR_ACCOUNT",
						"originalName": "CREATE_YOUR_ACCOUNT",
						"maskFrame": null,
						"layerFrame": {
							"x": 799,
							"y": 706,
							"width": 306,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "CREATE YOUR ACCOUNT",
							"css": [
								"/* CREATE YOUR ACCOUNT: */",
								"font-family: OpenSans-Bold;",
								"font-size: 26px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-CREATE_YOUR_ACCOUNT-q0fcnzfc.png",
							"frame": {
								"x": 799,
								"y": 706,
								"width": 306,
								"height": 20
							}
						},
						"children": []
					}
				]
			}
		]
	}
]
window.__imported__ = window.__imported__ || {};
window.__imported__["self-registration@1x/layers.json.js"] = [
	{
		"objectId": "51B20B96-65DF-43DF-B1CE-3AEF66F3F854",
		"kind": "artboard",
		"name": "Login_screen_landing_",
		"originalName": "Login screen (landing)",
		"maskFrame": null,
		"layerFrame": {
			"x": -512,
			"y": -483,
			"width": 1920,
			"height": 1080
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "C882E7EE-8067-4732-80DF-E9575F373072",
				"kind": "group",
				"name": "Login_form",
				"originalName": "Login form",
				"maskFrame": null,
				"layerFrame": {
					"x": 602,
					"y": 164,
					"width": 721,
					"height": 404
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Login_form-qzg4mku3.png",
					"frame": {
						"x": 602,
						"y": 164,
						"width": 721,
						"height": 404
					}
				},
				"children": [
					{
						"objectId": "5AA549EF-1052-416F-886E-681984AFF618",
						"kind": "group",
						"name": "btn_reset_password",
						"originalName": "btn-reset-password",
						"maskFrame": null,
						"layerFrame": {
							"x": 1098,
							"y": 440,
							"width": 183,
							"height": 23
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-btn_reset_password-nufbntq5.png",
							"frame": {
								"x": 1098,
								"y": 440,
								"width": 183,
								"height": 23
							}
						},
						"children": [
							{
								"objectId": "6EC4BCB6-BC67-4FD1-BC2D-B5C075EE38FC",
								"kind": "text",
								"name": "Reset_your_password",
								"originalName": "Reset_your_password",
								"maskFrame": null,
								"layerFrame": {
									"x": 1130,
									"y": 444.9999999999999,
									"width": 151,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "Reset your password",
									"css": [
										"/* Reset your password: */",
										"font-family: Lato-Black;",
										"font-size: 16px;",
										"color: #000000;"
									]
								},
								"image": {
									"path": "images/Layer-Reset_your_password-nkvdnejd.png",
									"frame": {
										"x": 1130,
										"y": 444.9999999999999,
										"width": 151,
										"height": 16
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "3219C27D-650C-47C0-AA42-39FC12877D1C",
						"kind": "group",
						"name": "Group_2",
						"originalName": "Group_2",
						"maskFrame": null,
						"layerFrame": {
							"x": 647,
							"y": 442,
							"width": 226,
							"height": 21
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2-mzixoumy.png",
							"frame": {
								"x": 647,
								"y": 442,
								"width": 226,
								"height": 21
							}
						},
						"children": [
							{
								"objectId": "92B6DF09-F04D-4ABE-8E0B-0EF2CDECB997",
								"kind": "text",
								"name": "Remember_my_login_details",
								"originalName": "Remember_my_login_details",
								"maskFrame": null,
								"layerFrame": {
									"x": 676,
									"y": 444.9999999999999,
									"width": 197,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "Remember my login details",
									"css": [
										"/* Remember my login details: */",
										"font-family: Lato-Black;",
										"font-size: 16px;",
										"color: #000000;"
									]
								},
								"image": {
									"path": "images/Layer-Remember_my_login_details-otjcnkrg.png",
									"frame": {
										"x": 676,
										"y": 444.9999999999999,
										"width": 197,
										"height": 16
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "AABC330F-045C-43F9-98F1-7D0C07F2FB7B",
						"kind": "text",
						"name": "Your_health_journey_starts_here",
						"originalName": "Your_health_journey_starts_here",
						"maskFrame": null,
						"layerFrame": {
							"x": 713,
							"y": 164,
							"width": 507,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "Your health journey starts here",
							"css": [
								"/* Your health journey starts here: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Your_health_journey_starts_here-qufcqzmz.png",
							"frame": {
								"x": 713,
								"y": 164,
								"width": 507,
								"height": 35
							}
						},
						"children": []
					},
					{
						"objectId": "745071AA-F9CA-4B3F-AA24-A408471410E9",
						"kind": "text",
						"name": "LOGIN",
						"originalName": "LOGIN",
						"maskFrame": null,
						"layerFrame": {
							"x": 920,
							"y": 520,
							"width": 79,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "LOGIN",
							"css": [
								"/* LOGIN: */",
								"font-family: OpenSans-Bold;",
								"font-size: 26px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-LOGIN-nzq1mdcx.png",
							"frame": {
								"x": 920,
								"y": 520,
								"width": 79,
								"height": 20
							}
						},
						"children": []
					},
					{
						"objectId": "88517F30-EC5E-48E2-9757-9F8742365756",
						"kind": "group",
						"name": "btn_toggle_password",
						"originalName": "btn-toggle-password",
						"maskFrame": null,
						"layerFrame": {
							"x": 1264,
							"y": 371,
							"width": 25,
							"height": 17
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-btn_toggle_password-odg1mtdg.png",
							"frame": {
								"x": 1264,
								"y": 371,
								"width": 25,
								"height": 17
							}
						},
						"children": []
					},
					{
						"objectId": "B0E3BF0D-3895-4F6D-825A-082B8FD16ACF",
						"kind": "group",
						"name": "enter_password_input",
						"originalName": "enter-password-input",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 341,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-enter_password_input-qjbfm0jg.png",
							"frame": {
								"x": 602,
								"y": 341,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "475EBC64-0E20-497C-A991-0AD42D4D39CB",
						"kind": "group",
						"name": "email_address_input",
						"originalName": "email-address-input",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 229,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-email_address_input-ndc1rujd.png",
							"frame": {
								"x": 602,
								"y": 229,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "83244D48-7DFF-4604-80D5-E12C08B64C76",
				"kind": "group",
				"name": "Registration_form",
				"originalName": "Registration form",
				"maskFrame": null,
				"layerFrame": {
					"x": 602,
					"y": 714,
					"width": 721,
					"height": 186
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "5B80874D-FB26-46AD-941B-62A1DEB468F5",
						"kind": "text",
						"name": "Heading",
						"originalName": "Heading",
						"maskFrame": null,
						"layerFrame": {
							"x": 664,
							"y": 714,
							"width": 609,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "New to member portal? Register here",
							"css": [
								"/* New to member portal? Register here: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Heading-nui4mdg3.png",
							"frame": {
								"x": 664,
								"y": 714,
								"width": 609,
								"height": 35
							}
						},
						"children": []
					},
					{
						"objectId": "B9FDDEFC-00C1-43AE-916E-F87CAF9CFD71",
						"kind": "group",
						"name": "email_input",
						"originalName": "email-input",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 779,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-email_input-qjlgrerf.png",
							"frame": {
								"x": 602,
								"y": 779,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "50B6C109-17F9-4928-8028-0B16E2B85675",
						"kind": "group",
						"name": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_",
						"originalName": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account.",
						"maskFrame": null,
						"layerFrame": {
							"x": 681,
							"y": 874,
							"width": 517,
							"height": 26
						},
						"visible": true,
						"metadata": {
							"opacity": 0.92
						},
						"image": {
							"path": "images/Layer-Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_-ntbcnkmx.png",
							"frame": {
								"x": 681,
								"y": 874,
								"width": 517,
								"height": 26
							}
						},
						"children": []
					},
					{
						"objectId": "CAB71B8C-781C-42AC-A06D-2F0A44FB5568",
						"kind": "group",
						"name": "btn_create_your_account",
						"originalName": "btn-create-your-account",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 924,
							"width": 721,
							"height": 77
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-btn_create_your_account-q0fcnzfc.png",
							"frame": {
								"x": 602,
								"y": 924,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "594A05C6-D3F8-4EF2-8BE0-6554F9A07B95",
				"kind": "group",
				"name": "Divider",
				"originalName": "Divider",
				"maskFrame": null,
				"layerFrame": {
					"x": 517,
					"y": 648,
					"width": 892,
					"height": 3
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Divider-ntk0qta1.png",
					"frame": {
						"x": 517,
						"y": 648,
						"width": 892,
						"height": 3
					}
				},
				"children": []
			},
			{
				"objectId": "4796D7C2-F1E9-4047-B2EE-C2FFA13B71D6",
				"kind": "group",
				"name": "Canvas",
				"originalName": "Canvas",
				"maskFrame": null,
				"layerFrame": {
					"x": 2,
					"y": 0,
					"width": 1920,
					"height": 1080
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Canvas-ndc5nkq3.png",
					"frame": {
						"x": 2,
						"y": 0,
						"width": 1920,
						"height": 1080
					}
				},
				"children": []
			}
		]
	},
	{
		"objectId": "20B5629D-A0BD-45AC-AEBB-7F9357C3ECB8",
		"kind": "artboard",
		"name": "$2factor",
		"originalName": "2factor",
		"maskFrame": null,
		"layerFrame": {
			"x": 1508,
			"y": -483,
			"width": 1920,
			"height": 1080
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "752B5B02-1B2A-4EEF-9B98-B51B7C65B931",
				"kind": "group",
				"name": "User_IDVerification_Two_factor_Email_update",
				"originalName": "User IDVerification (Two factor) – Email update",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1920,
					"height": 1080
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-User_IDVerification_Two_factor_Email_update-nzuyqjvc.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1920,
						"height": 1080
					}
				},
				"children": [
					{
						"objectId": "C4256B9C-95BF-47D2-B47B-6A9A7E10239A",
						"kind": "group",
						"name": "You_can_also_verify_your_identity_by_answering_a_few_questions",
						"originalName": "You_can_also_verify_your_identity_by_answering_a_few_questions",
						"maskFrame": null,
						"layerFrame": {
							"x": 666,
							"y": 649,
							"width": 544,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 0.92
						},
						"image": {
							"path": "images/Layer-You_can_also_verify_your_identity_by_answering_a_few_questions-qzqyntzc.png",
							"frame": {
								"x": 666,
								"y": 649,
								"width": 544,
								"height": 20
							}
						},
						"children": []
					},
					{
						"objectId": "3E5509A7-4F96-4732-9402-6282195E60B5",
						"kind": "group",
						"name": "Haven_t_received_the_code_Our_agents_are_ready_to_help_you_get_it_sorted_open_a_web_chat",
						"originalName": "Haven_t_received_the_code_Our_agents_are_ready_to_help_you_get_it_sorted_open_a_web_chat",
						"maskFrame": null,
						"layerFrame": {
							"x": 1374,
							"y": 542,
							"width": 296,
							"height": 50
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Haven_t_received_the_code_Our_agents_are_ready_to_help_you_get_it_sorted_open_a_web_chat-m0u1nta5.png",
							"frame": {
								"x": 1374,
								"y": 542,
								"width": 296,
								"height": 50
							}
						},
						"children": []
					},
					{
						"objectId": "A14DF601-F4B8-43AF-B781-DB4C37C94339",
						"kind": "group",
						"name": "Group_4",
						"originalName": "Group_4",
						"maskFrame": null,
						"layerFrame": {
							"x": 576,
							"y": 554,
							"width": 196,
							"height": 31
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_4-qte0rey2.png",
							"frame": {
								"x": 576,
								"y": 554,
								"width": 196,
								"height": 31
							}
						},
						"children": [
							{
								"objectId": "A47C36B5-66B6-4EEB-A5D0-DC2DC552B7AA",
								"kind": "text",
								"name": "_04XX_XXX_334",
								"originalName": "_04XX_XXX_334",
								"maskFrame": null,
								"layerFrame": {
									"x": 608,
									"y": 560,
									"width": 164,
									"height": 19
								},
								"visible": true,
								"metadata": {
									"opacity": 0.92,
									"string": "04XX XXX 334",
									"css": [
										"/* 04XX XXX 334: */",
										"font-family: Lato-Black;",
										"font-size: 24px;",
										"color: #000000;"
									]
								},
								"image": {
									"path": "images/Layer-_04XX_XXX_334-qtq3qzm2.png",
									"frame": {
										"x": 608,
										"y": 560,
										"width": 164,
										"height": 19
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "C99BE172-BC52-4011-B436-7AA232E6AAB4",
						"kind": "group",
						"name": "A_unique_6_digit_pin_has_been_sent_to_the_registered_mobile_phone_number_on_your_membership_please_enter_that_code_below_to_continue",
						"originalName": "A_unique_6_digit_pin_has_been_sent_to_the_registered_mobile_phone_number_on_your_membership_please_enter_that_code_below_to_continue",
						"maskFrame": null,
						"layerFrame": {
							"x": 584,
							"y": 445,
							"width": 704,
							"height": 43
						},
						"visible": true,
						"metadata": {
							"opacity": 0.92
						},
						"image": {
							"path": "images/Layer-A_unique_6_digit_pin_has_been_sent_to_the_registered_mobile_phone_number_on_your_membership_please_enter_that_code_below_to_continue-qzk5qkux.png",
							"frame": {
								"x": 584,
								"y": 445,
								"width": 704,
								"height": 43
							}
						},
						"children": []
					},
					{
						"objectId": "E29214A2-6709-412A-8A95-220EF1C869E5",
						"kind": "text",
						"name": "CONTINUE",
						"originalName": "CONTINUE",
						"maskFrame": null,
						"layerFrame": {
							"x": 865,
							"y": 746,
							"width": 135,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "CONTINUE",
							"css": [
								"/* CONTINUE: */",
								"font-family: OpenSans-Bold;",
								"font-size: 26px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-CONTINUE-rti5mje0.png",
							"frame": {
								"x": 865,
								"y": 746,
								"width": 135,
								"height": 20
							}
						},
						"children": []
					},
					{
						"objectId": "0CEBE2BD-B6D4-42FD-911E-94E2FA4D565B",
						"kind": "text",
						"name": "Enter_the_verification_code",
						"originalName": "Enter_the_verification_code",
						"maskFrame": null,
						"layerFrame": {
							"x": 857,
							"y": 562,
							"width": 230,
							"height": 16
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Enter the verification code",
							"css": [
								"/* Enter the verification code: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Enter_the_verification_code-menfqkuy.png",
							"frame": {
								"x": 857,
								"y": 562,
								"width": 230,
								"height": 16
							}
						},
						"children": []
					},
					{
						"objectId": "74F8280E-DE18-40F7-952C-A1F02471B920",
						"kind": "group",
						"name": "Rectangle_1",
						"originalName": "Rectangle_1",
						"maskFrame": null,
						"layerFrame": {
							"x": 811,
							"y": 531,
							"width": 486,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_1-nzrgodi4.png",
							"frame": {
								"x": 811,
								"y": 531,
								"width": 486,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "F39B9168-2D85-4511-83DA-6DA9DF5701BB",
						"kind": "text",
						"name": "We_just_need_to_verify_your_identity",
						"originalName": "We_just_need_to_verify_your_identity",
						"maskFrame": null,
						"layerFrame": {
							"x": 636,
							"y": 346,
							"width": 579,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "We just need to verify your identity",
							"css": [
								"/* We just need to verify your identity: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-We_just_need_to_verify_your_identity-rjm5qjkx.png",
							"frame": {
								"x": 636,
								"y": 346,
								"width": 579,
								"height": 35
							}
						},
						"children": []
					}
				]
			}
		]
	}
]
if (DeviceComponent) {DeviceComponent.Devices["iphone-6-silver"].deviceImageJP2 = false};
if (window.Framer) {window.Framer.Defaults.DeviceView = {"deviceScale":"fit","selectedHand":"","deviceType":"custom","contentScale":1,"hideBezel":true,"orientation":0};
}
if (window.Framer) {window.Framer.Defaults.DeviceComponent = {"deviceScale":"fit","selectedHand":"","deviceType":"custom","contentScale":1,"hideBezel":true,"orientation":0};
}
window.FramerStudioInfo = {"deviceImagesUrl":"\/_server\/resources\/DeviceImages","documentTitle":"self-registration.framer"};

Framer.Device = new Framer.DeviceView();
Framer.Device.setupContext();