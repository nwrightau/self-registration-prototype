window.__imported__ = window.__imported__ || {};
window.__imported__["self-registration@1.5x/layers.json.js"] = [
	{
		"objectId": "51B20B96-65DF-43DF-B1CE-3AEF66F3F854",
		"kind": "artboard",
		"name": "Login_screen_landing_",
		"originalName": "Login screen (landing)",
		"maskFrame": null,
		"layerFrame": {
			"x": -512,
			"y": -483,
			"width": 1920,
			"height": 1080
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"image": {
			"path": "images/Layer-Login_screen_landing_-ntfcmjbc.png",
			"frame": {
				"x": -512,
				"y": -483,
				"width": 1920,
				"height": 1080
			}
		},
		"children": [
			{
				"objectId": "C882E7EE-8067-4732-80DF-E9575F373072",
				"kind": "group",
				"name": "Login_form",
				"originalName": "Login form",
				"maskFrame": null,
				"layerFrame": {
					"x": 602,
					"y": 164,
					"width": 721,
					"height": 404
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Login_form-qzg4mku3.png",
					"frame": {
						"x": 602,
						"y": 164,
						"width": 721,
						"height": 404
					}
				},
				"children": [
					{
						"objectId": "5AA549EF-1052-416F-886E-681984AFF618",
						"kind": "group",
						"name": "Group_1",
						"originalName": "Group_1",
						"maskFrame": null,
						"layerFrame": {
							"x": 1098,
							"y": 440,
							"width": 183,
							"height": 23
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_1-nufbntq5.png",
							"frame": {
								"x": 1098,
								"y": 440,
								"width": 183,
								"height": 23
							}
						},
						"children": [
							{
								"objectId": "6EC4BCB6-BC67-4FD1-BC2D-B5C075EE38FC",
								"kind": "text",
								"name": "Reset_your_password",
								"originalName": "Reset_your_password",
								"maskFrame": null,
								"layerFrame": {
									"x": 1130,
									"y": 444.9999999999999,
									"width": 151,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "Reset your password",
									"css": [
										"/* Reset your password: */",
										"font-family: Lato-Black;",
										"font-size: 16px;",
										"color: #000000;"
									]
								},
								"image": {
									"path": "images/Layer-Reset_your_password-nkvdnejd.png",
									"frame": {
										"x": 1130,
										"y": 444.9999999999999,
										"width": 151,
										"height": 16
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "3219C27D-650C-47C0-AA42-39FC12877D1C",
						"kind": "group",
						"name": "Group_2",
						"originalName": "Group_2",
						"maskFrame": null,
						"layerFrame": {
							"x": 647,
							"y": 442,
							"width": 226,
							"height": 21
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2-mzixoumy.png",
							"frame": {
								"x": 647,
								"y": 442,
								"width": 226,
								"height": 21
							}
						},
						"children": [
							{
								"objectId": "92B6DF09-F04D-4ABE-8E0B-0EF2CDECB997",
								"kind": "text",
								"name": "Remember_my_login_details",
								"originalName": "Remember_my_login_details",
								"maskFrame": null,
								"layerFrame": {
									"x": 676,
									"y": 444.9999999999999,
									"width": 197,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "Remember my login details",
									"css": [
										"/* Remember my login details: */",
										"font-family: Lato-Black;",
										"font-size: 16px;",
										"color: #000000;"
									]
								},
								"image": {
									"path": "images/Layer-Remember_my_login_details-otjcnkrg.png",
									"frame": {
										"x": 676,
										"y": 444.9999999999999,
										"width": 197,
										"height": 16
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "AABC330F-045C-43F9-98F1-7D0C07F2FB7B",
						"kind": "text",
						"name": "Your_health_journey_starts_here",
						"originalName": "Your_health_journey_starts_here",
						"maskFrame": null,
						"layerFrame": {
							"x": 713,
							"y": 164,
							"width": 507,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "Your health journey starts here",
							"css": [
								"/* Your health journey starts here: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Your_health_journey_starts_here-qufcqzmz.png",
							"frame": {
								"x": 713,
								"y": 164,
								"width": 507,
								"height": 35
							}
						},
						"children": []
					},
					{
						"objectId": "745071AA-F9CA-4B3F-AA24-A408471410E9",
						"kind": "text",
						"name": "LOGIN",
						"originalName": "LOGIN",
						"maskFrame": null,
						"layerFrame": {
							"x": 920,
							"y": 520,
							"width": 79,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "LOGIN",
							"css": [
								"/* LOGIN: */",
								"font-family: OpenSans-Bold;",
								"font-size: 26px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-LOGIN-nzq1mdcx.png",
							"frame": {
								"x": 920,
								"y": 520,
								"width": 79,
								"height": 20
							}
						},
						"children": []
					},
					{
						"objectId": "B0E3BF0D-3895-4F6D-825A-082B8FD16ACF",
						"kind": "text",
						"name": "Enter_your_password",
						"originalName": "Enter_your_password",
						"maskFrame": null,
						"layerFrame": {
							"x": 648,
							"y": 372,
							"width": 179,
							"height": 19
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Enter your password",
							"css": [
								"/* Enter your password: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Enter_your_password-qjbfm0jg.png",
							"frame": {
								"x": 648,
								"y": 372,
								"width": 179,
								"height": 19
							}
						},
						"children": []
					},
					{
						"objectId": "179FE9B0-BEE9-4780-BD92-88A706AC493C",
						"kind": "group",
						"name": "Rectangle_2",
						"originalName": "Rectangle_2",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 341,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_2-mtc5rku5.png",
							"frame": {
								"x": 602,
								"y": 341,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "475EBC64-0E20-497C-A991-0AD42D4D39CB",
						"kind": "text",
						"name": "Enter_your_email_address_2",
						"originalName": "Enter_your_email_address-2",
						"maskFrame": null,
						"layerFrame": {
							"x": 648,
							"y": 260,
							"width": 214,
							"height": 19
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Enter your email address",
							"css": [
								"/* Enter your email address: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Enter_your_email_address_2-ndc1rujd.png",
							"frame": {
								"x": 648,
								"y": 260,
								"width": 214,
								"height": 19
							}
						},
						"children": []
					},
					{
						"objectId": "83443C9E-9122-469B-B700-E63C0E5B8058",
						"kind": "group",
						"name": "Rectangle_1",
						"originalName": "Rectangle_1",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 229,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_1-odm0ndnd.png",
							"frame": {
								"x": 602,
								"y": 229,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "83244D48-7DFF-4604-80D5-E12C08B64C76",
				"kind": "group",
				"name": "Registration_form",
				"originalName": "Registration form",
				"maskFrame": null,
				"layerFrame": {
					"x": 602,
					"y": 714,
					"width": 721,
					"height": 186
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "B9FDDEFC-00C1-43AE-916E-F87CAF9CFD71",
						"kind": "text",
						"name": "Enter_your_email_address",
						"originalName": "Enter_your_email_address",
						"maskFrame": null,
						"layerFrame": {
							"x": 648,
							"y": 810,
							"width": 214,
							"height": 19
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Enter your email address",
							"css": [
								"/* Enter your email address: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Enter_your_email_address-qjlgrerf.png",
							"frame": {
								"x": 648,
								"y": 810,
								"width": 214,
								"height": 19
							}
						},
						"children": []
					},
					{
						"objectId": "F0AD7474-1D3F-4F13-9F6D-91D41A631B74",
						"kind": "group",
						"name": "Rectangle_4",
						"originalName": "Rectangle_4",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 779,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_4-rjbbrdc0.png",
							"frame": {
								"x": 602,
								"y": 779,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "50B6C109-17F9-4928-8028-0B16E2B85675",
						"kind": "group",
						"name": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_",
						"originalName": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account.",
						"maskFrame": null,
						"layerFrame": {
							"x": 681,
							"y": 874,
							"width": 517,
							"height": 26
						},
						"visible": true,
						"metadata": {
							"opacity": 0.92
						},
						"image": {
							"path": "images/Layer-Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_-ntbcnkmx.png",
							"frame": {
								"x": 681,
								"y": 874,
								"width": 517,
								"height": 26
							}
						},
						"children": []
					},
					{
						"objectId": "5B80874D-FB26-46AD-941B-62A1DEB468F5",
						"kind": "text",
						"name": "New_to_member_portal_Register_here",
						"originalName": "New_to_member_portal_Register_here",
						"maskFrame": null,
						"layerFrame": {
							"x": 664,
							"y": 714,
							"width": 609,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "New to member portal? Register here",
							"css": [
								"/* New to member portal? Register here: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-New_to_member_portal_Register_here-nui4mdg3.png",
							"frame": {
								"x": 664,
								"y": 714,
								"width": 609,
								"height": 35
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "F2E98239-734D-4B67-8310-3196F996E891",
		"kind": "artboard",
		"name": "Login_screen_register_activce_",
		"originalName": "Login screen (register activce)",
		"maskFrame": null,
		"layerFrame": {
			"x": 1508,
			"y": -483,
			"width": 1920,
			"height": 1080
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "0FB5E997-6A38-4728-B016-0E08EA98A724",
				"kind": "group",
				"name": "Login_screen_register_active_",
				"originalName": "Login screen (register active)",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1920,
					"height": 1080
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Login_screen_register_active_-mezcnuu5.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1920,
						"height": 1080
					}
				},
				"children": [
					{
						"objectId": "3B580AC5-BAFF-4D9F-98E1-22E90DF4F1AD",
						"kind": "text",
						"name": "Email_address",
						"originalName": "Email_address",
						"maskFrame": null,
						"layerFrame": {
							"x": 645,
							"y": 487,
							"width": 125,
							"height": 17
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "Email address",
							"css": [
								"/* Email address: */",
								"font-family: Lato-Black;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-Email_address-m0i1odbb.png",
							"frame": {
								"x": 645,
								"y": 487,
								"width": 125,
								"height": 17
							}
						},
						"children": []
					},
					{
						"objectId": "B39E63B4-366B-4ED1-B300-09AA50AE68D8",
						"kind": "group",
						"name": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_1",
						"originalName": "Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account.",
						"maskFrame": null,
						"layerFrame": {
							"x": 681,
							"y": 611,
							"width": 517,
							"height": 26
						},
						"visible": true,
						"metadata": {
							"opacity": 0.92
						},
						"image": {
							"path": "images/Layer-Only_the_primary_policy_holder_can_register_for_member_portal_other_users_will_need_to_be_invited_by_the_primary_policy_holder_after_they_have_created_their_account_-qjm5rtyz.png",
							"frame": {
								"x": 681,
								"y": 611,
								"width": 517,
								"height": 26
							}
						},
						"children": []
					},
					{
						"objectId": "87613974-3455-43E2-A970-FB150E11E19E",
						"kind": "text",
						"name": "jkonto_gmail_com",
						"originalName": "jkonto_gmail.com",
						"maskFrame": null,
						"layerFrame": {
							"x": 645,
							"y": 547,
							"width": 161,
							"height": 19
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "jkonto@gmail.com",
							"css": [
								"/* jkonto@gmail.com: */",
								"font-family: Lato-Regular;",
								"font-size: 20px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-jkonto_gmail_com-odc2mtm5.png",
							"frame": {
								"x": 645,
								"y": 547,
								"width": 161,
								"height": 19
							}
						},
						"children": []
					},
					{
						"objectId": "69351AAF-C3FF-44B6-8871-8A2EBEEA322E",
						"kind": "group",
						"name": "Rectangle_41",
						"originalName": "Rectangle_4",
						"maskFrame": null,
						"layerFrame": {
							"x": 602,
							"y": 516,
							"width": 721,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_4-njkzntfb.png",
							"frame": {
								"x": 602,
								"y": 516,
								"width": 721,
								"height": 77
							}
						},
						"children": []
					},
					{
						"objectId": "DE00769D-3DB0-4A6C-A000-2AE631FA2351",
						"kind": "text",
						"name": "New_to_member_portal_Register_here1",
						"originalName": "New_to_member_portal_Register_here",
						"maskFrame": null,
						"layerFrame": {
							"x": 664,
							"y": 407,
							"width": 609,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 0.916,
							"string": "New to member portal? Register here",
							"css": [
								"/* New to member portal? Register here: */",
								"font-family: Lato-Black;",
								"font-size: 36px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-New_to_member_portal_Register_here-reuwmdc2.png",
							"frame": {
								"x": 664,
								"y": 407,
								"width": 609,
								"height": 35
							}
						},
						"children": []
					},
					{
						"objectId": "CAB71B8C-781C-42AC-A06D-2F0A44FB5568",
						"kind": "text",
						"name": "CREATE_YOUR_ACCOUNT",
						"originalName": "CREATE_YOUR_ACCOUNT",
						"maskFrame": null,
						"layerFrame": {
							"x": 799,
							"y": 706,
							"width": 306,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "CREATE YOUR ACCOUNT",
							"css": [
								"/* CREATE YOUR ACCOUNT: */",
								"font-family: OpenSans-Bold;",
								"font-size: 26px;",
								"color: #000000;"
							]
						},
						"image": {
							"path": "images/Layer-CREATE_YOUR_ACCOUNT-q0fcnzfc.png",
							"frame": {
								"x": 799,
								"y": 706,
								"width": 306,
								"height": 20
							}
						},
						"children": []
					}
				]
			}
		]
	}
]